from sklearn.cluster import AgglomerativeClustering
import dsgen.models

def name_cluster(names: list[str], size:int) -> list[list[int]]:
    print(f"Encoding all names, length {len(names)}...")
    encoded = dsgen.models.encoder_model.encode(names, normalize_embeddings=True)
    clusterer = AgglomerativeClustering(size, affinity='cosine', linkage='complete')
    print(f"Clustering all names, length {len(names)}, {size} clusters...")
    clusterer.fit(encoded)
    cluster_result = []
    for idx,label in enumerate(iter(clusterer.labels_)):
        while len(cluster_result) <= label:
            cluster_result.append(list())
        cluster_result[label].append(idx)
    print("Done")
    return cluster_result
