from dataclasses import dataclass
from typing import Any
import torch

from spacy.tokens.doc import Doc
from pathos.multiprocessing import ProcessPool, ProcessingPool
from pathos.pp import ParallelPool
#import multiprocessing

from dsgen.metrics import acceptably_similar
from .models import encoder_model, spacy_model
from sentence_transformers import util
from .parse import parse, ParsedClause
from functools import cache
from .dobj import swappable_dobj_pairs

torch.set_num_threads(1)

@dataclass
class Recipe:
    title: str
    steps: list[str]

@dataclass
class QuerySet:
    recipe_1: Recipe
    recipe_2: Recipe
    queries: list[tuple[str, str]]

# takes in a tuple of spacy-processed objects.
def generate_clause(steps: list[Doc]):
    return _generate_clause(tuple(steps))

@cache
def _generate_clause(steps: tuple) -> list[ParsedClause]:
    clauses = []
    for step in steps:
        for clause in parse(step):
            clauses.append(clause)
    return clauses

def new_clause_combination(clause_orig: ParsedClause, verb:str|None = None, pairs:list[tuple[int,str]] | None = None) -> ParsedClause:
    clause_new = clause_orig.copy()
    if verb:
        clause_new.verb = verb
    if pairs:
        for i,new_phrase in pairs:
            clause_new.direct_objects[1][i] = new_phrase
    return clause_new

# Replace verb or direct object (or both).
def swap_parsed(clause_orig: ParsedClause, clause_new: ParsedClause) -> list[ParsedClause]:
    # not operable, so we skip.
    if not clause_orig.operable() or not clause_new.operable():
        return []

    # the answer.
    merges = []
    verbs_swappable, dobjs_swappable = False, False

    # check if verbs are similar enough, and if they are, add the swapped query.
    _verb_encodings: Any = encoder_model.encode([clause_orig.verb, clause_new.verb])
    _verb_similarity = float(util.cos_sim(_verb_encodings, _verb_encodings)[0][1])
    if acceptably_similar(_verb_similarity):
        merges.append(new_clause_combination(clause_orig, verb=clause_new.verb, pairs=None))
        verbs_swappable = True

    # If swappable pairs are found, then substitute.
    dobj_pairs = swappable_dobj_pairs(clause_orig.direct_objects[1], clause_new.direct_objects[1])
    if dobj_pairs:
        merges.append(new_clause_combination(clause_orig, verb=None, pairs=dobj_pairs))
        dobjs_swappable = True

    # If we can both swap verbs and direct objects, why not do both?
    if verbs_swappable and dobjs_swappable:
        merges.append(new_clause_combination(clause_orig, verb=clause_new.verb, pairs=dobj_pairs))

    return merges


def pairwise_queries(names: tuple[str,str], steps: tuple[list[ParsedClause],list[ParsedClause]]) -> QuerySet:
    print(f"Working on recipes {names[0]} and {names[1]}...")
    queries: list[tuple[str, str]] = []
    for clause_orig in steps[0]:
        if not clause_orig.operable():
            continue
        for clause_new in steps[1]:
            if not clause_new.operable():
                continue
            for merged in swap_parsed(clause_orig, clause_new):
                queries.append((clause_orig.show(), merged.show()))
    return QuerySet(
        recipe_1=Recipe(names[0], list(map(lambda x: x.show(), steps[0]))),
        recipe_2=Recipe(names[1], list(map(lambda x: x.show(), steps[1]))),
        queries=queries
    )

def cluster_queries(names: list[str | Any], steps: list[list[str] | Any], processes:int=1):
    assert len(names) == len(steps)
    print("Turning all steps to a Doc...")
    clauses = []

    def generate_clause_wrapper(args):
        idx, recipe = args
        print(f"Running Doc and generate_clause conversion for {idx}...")
        return generate_clause([spacy_model(step) for step in recipe])

    #with multiprocessing.get_context('spawn').Pool(processes) as p:
    with ProcessPool(processes) as p:
        print(f"Running Doc and generate_clause conversions with {processes} threads...")
        clauses = p.map(generate_clause_wrapper, enumerate(steps))
        print("Done")

    def pairwise_queries_wrapper(args):
        i, j = args
        return pairwise_queries((names[i], names[j]), (clauses[i], clauses[j]))

    res = [(i, j) for i in range(len(clauses)) for j in range(len(clauses)) if i != j]

    print(f"Generating pairwise queries with 1 thread :-( ...")

    # lazy-eval
    res = map(pairwise_queries_wrapper, res)

    # KEEPS DEADLOCKING!!!
    #with ProcessPool(processes) as p:
    #    res = p.map(pairwise_queries_wrapper, res)

    print("Done")
    return res
