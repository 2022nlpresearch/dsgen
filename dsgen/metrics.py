def acceptably_similar(x):
    return 0.4 < x < 0.83
