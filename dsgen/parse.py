from spacy import symbols
from dataclasses import dataclass, field
from copy import deepcopy


@dataclass
class OtherPhrase:
    text: str
    include: bool = True


@dataclass
class ParsedClause:
    text: str
    verb: str = ""
    direct_objects: tuple[int, list[str]] = (0, [])
    other: list[tuple[int, OtherPhrase]] = field(default_factory=list)

    def operable(self):
        return bool(self.verb)

    def show(self):
        # I didn't put a not here and wasted 1 hour debugging :-(
        if not self.operable():
            return self.text
        dobjs = self.direct_objects[1]
        if len(dobjs):
            direct_objects_str = ", ".join(dobjs[:-1]) + ((", and " if
                    len(dobjs) > 1 else "") +
                    dobjs[-1])
        else:
            direct_objects_str = ""
        return self.verb + " " + direct_objects_str + " " + " ".join([el[1].text for el in self.other if el[1].include])

    def copy(self):
        text = self.text[:]
        verb = self.verb[:]
        direct_objects = (self.direct_objects[0], self.direct_objects[1][:])
        other = deepcopy(self.other)
        return ParsedClause(text, verb, direct_objects, other)

def _iterator_to_phrase(subtree) -> str:
    return " ".join(map(str, subtree))

# takes a token tree and returns all of the nouns in it (as a set).
def _noun_lists(token) -> list[str]:
    res = []
    current_element = []
    for child in token.subtree:
        if child == token:
            current_element.append(token.text)
        elif child.head == token:
            if child.dep == symbols.conj:
                # there should be only one
                res = _noun_lists(child)
            elif not child.dep == symbols.cc:
                subtree = filter(lambda x: x.is_alpha, child.subtree)
                current_element.append(_iterator_to_phrase(subtree))
    return [_iterator_to_phrase(current_element)] + res


def _child_to_phrase(child) -> OtherPhrase:
    # make sure the subtree phrase is not a stub.
    include = not (len(list(child.subtree)) == 1 and child.is_stop)
    return OtherPhrase(_iterator_to_phrase(child.subtree), include)


def _parse(token) -> list[ParsedClause]:
    if token.pos != symbols.VERB or token.is_stop:
        return [ParsedClause(_iterator_to_phrase(token.subtree))]

    dobj = (0, list())
    other = []
    other_clauses: list[ParsedClause] = []
    for idx, child in enumerate(token.children):
        if not child.is_alpha or child.dep == symbols.cc:
            continue
        # direct object processing
        if child.dep == symbols.dobj:
            if child.is_stop:
                dobj = (idx, list())
            else:
                dobj = (idx, _noun_lists(child))
        # this sentence is compound (or complementary), so parse the next independent clause.
        elif child.dep in (symbols.conj, symbols.xcomp) and child.pos == symbols.VERB:
            for result in _parse(child):
                other_clauses.append(result)
        else:
            other.append((idx, _child_to_phrase(child)))
    return [ParsedClause(_iterator_to_phrase(token.subtree), token.lemma_, dobj, other)] + other_clauses


def parse(doc) -> list[ParsedClause]:
    res = []
    for sent in doc.sents:
        for clause in _parse(sent.root):
            res.append(clause)
    return res
