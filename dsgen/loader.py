from dataclasses import dataclass
from pathlib import Path
from typing_extensions import Self

from pandas import read_csv
from ast import literal_eval

@dataclass
class RecipeBatch:
    names: list[str]
    steps: list[list[str]]
    def apply_clusters(self, clusters: list[list[int]]) -> list[Self]:
        return [
            RecipeBatch(
                [self.names[idx] for idx in cluster],
                [self.steps[idx] for idx in cluster]
            )
            for cluster in clusters
        ]


def load_foodcom(csv_path: Path, start:int=0, size:int=600) -> RecipeBatch:
    frame = read_csv(csv_path)[start:start+size]
    frame['steps'] = frame['steps'].apply(literal_eval)
    return RecipeBatch(frame['name'], frame['steps'])
