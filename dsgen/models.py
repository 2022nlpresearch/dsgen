import spacy
import sentence_transformers

print("Loading models...")
spacy_model = spacy.load('en_core_web_trf')
encoder_model = sentence_transformers.SentenceTransformer('multi-qa-MiniLM-L6-cos-v1')
print("Done")
