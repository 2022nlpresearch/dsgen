from typing import Any
from .models import encoder_model
from sentence_transformers import util
from functools import cache
from .metrics import acceptably_similar


# pass in as a tuple b/c cache demands it.
@cache
def embeddings_dobj(objects:tuple[str]):
    return encoder_model.encode(list(objects))


# Take two lists of direct objects and figure the best (hopefully lol)
# way to make swappable pairs.
def swappable_dobj_pairs(dobj_old: list[str], dobj_new: list[str]) -> 'list[tuple[int,str]]':
    if not dobj_old or not dobj_new:
        return []
    encodings_orig: Any = embeddings_dobj(tuple(dobj_old))
    encodings_new: Any = embeddings_dobj(tuple(dobj_new))
    # dot score is more efficient than cosine similarity (also models spit out unit vectors)
    similarities = util.dot_score(encodings_orig, encodings_new)

    # contains a 2d array of (index to keep track of, cosine similarity)
    preferred_old: list[set[tuple[int,float]]] = [set() for _ in range(len(encodings_orig))]
    preferred_new: list[set[tuple[int,float]]] = [set() for _ in range(len(encodings_new))]

    for i in range(len(encodings_orig)):
        to_float = map(float, similarities[i])
        # filter exact copies out (but also stuff that is not related).
        filtered = filter(lambda x: acceptably_similar(x[1]), enumerate(to_float))
        filtered = sorted(filtered, key=lambda x: -x[1])
        # take the **indices** of the at most two best choices.
        preferred_old[i] = set(map(lambda tup: tup[0], zip(filtered, range(2))))
        # add these choices to candidates for the preferred_new.
        for j, sim in preferred_old[i]:
            preferred_new[j].add((i, sim))

    # each new word prunes all old words that don't have maximum similarity with
    # itself.
    for j in range(len(encodings_new)):
        eliminate = list(sorted(preferred_new[j], key=lambda x: -x[1]))[1:]
        for i, sim in eliminate:
            preferred_old[i].discard((j, sim))

    res = []
    for i in range(len(encodings_orig)):
        if len(preferred_old[i]):
            # pick the choice with the greatest similarity.
            choice = list(sorted(preferred_old[i], key=lambda x: -x[1]))[0]
            new_phrase = dobj_new[choice[0]]
            res.append((i, new_phrase))
    return res
