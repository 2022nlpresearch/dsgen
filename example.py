#!/usr/bin/env python3

from dsgen.queries import cluster_queries
from pprint import pprint

recipes = [
    ['wash and coarsely chop mushrooms', 'saute over medium-low heat , covered , with lemon juice , butter , onions , and water for 20 minutes', 'transfer to a large saucepan , add the broth , and bring to a low simmer', 'blend the sour cream with the flour and mix well', 'blend in the 1 cup reserved stock until smooth', 'add slowly to the pot , stirring constantly', 'simmer for five minutes , stirring often', 'serve garnished with chopped dill', 'the richly flavored wild mushrooms that polish cooks used to use are difficult to find', 'you may substitute dried mushrooms for part of the fresh , reconstituted according to package directions , for richer flavor'],
    ['in a 4 quart pot , saut the mushrooms in butter till tender', 'add garlic', 'saut on low heat 5 minutes', 'in a blender , pure the cashews with the water until very smooth', 'add to the mushrooms', 'add the bouillon and cook on low heat , stirring frequently , until the soup begins to thicken', 'add parsley and nutmeg', 'serve']
]

titles = ['zupa ze swiezych grzybow  polish mushroom soup', 'cream  of mushroom soup']

res = list(cluster_queries(titles, recipes))

pprint(res[0].recipe_1)
pprint(res[0].recipe_2)

for x in res:
    pprint(x.queries)
