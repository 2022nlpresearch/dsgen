# Dataset Generator

This is a library (with some examples) for generating a dataset of possibly
swappable recipes.

## foodcom.py

You need to provide (in the cli arguments) a path to `RAW_recipes.csv` as
specified in [this
dataset](https://www.kaggle.com/datasets/shuyangli94/food-com-recipes-and-user-interactions). Alternatively, hack the script to your liking.

## Output folder format

Data points are stored in `$output_folder/(hash)/(number)` (e.g.
`output/18f4cf4a41a23297417382fc803f3cf9c840632e3c9361aff1c1d9c6946b69fc/10`).

Each data file contains JSON structured as follows:

```json
{
  "recipe_1": {
    "title": "Title of First Recipe (to be overlaid, serves as the basis for queries)",
    "steps": ["list", "of", "recipe", "steps", "from the dataset"]
  },
  "recipe_2": {
    "title": "Title of Second Recipe",
    "steps": ["list", "of", "recipe", "steps", "from the dataset"]
  },
  "queries": [
    ["Original Step (format modified to fit structure)", "Modified step"],
    ["Original Step (format modified to fit structure) 2", "Modified step 2"],
    ["Original Step (format modified to fit structure) 3", "Modified step 3"],
    ...
  ]
}
```
