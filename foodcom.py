#!/usr/bin/env python3

import argparse
from pathlib import Path
import json
import os
from hashlib import sha256


# apparently we need this set.
os.environ["TOKENIZERS_PARALLELISM"] = "true"

def hash_names(names: list[str]):
    res = sha256()
    for t in names:
        res.update(bytes(t, "utf-8"))
    return res.hexdigest()

def main(args):
    from dsgen.cluster import name_cluster
    from dsgen.loader import RecipeBatch, load_foodcom
    from dsgen.queries import cluster_queries
    dataset = load_foodcom(args.path, 0, args.num_recipes)
    output: Path = args.output
    os.makedirs(output, exist_ok=True)
    separated = dataset.apply_clusters(name_cluster(dataset.names, args.num_clusters))
    def process(arguments: tuple[int,RecipeBatch]):
        print(f"Working on batch {arguments[0]} of size {len(arguments[1].names)}...")
        batch = arguments[1]
        save_path = output / hash_names(batch.names)
        os.makedirs(save_path, exist_ok=True)
        for idx, result in enumerate(cluster_queries(batch.names, batch.steps, args.processes)):
            if not result.queries:
                continue
            payload = dict(
                recipe_1=dict(title=result.recipe_1.title, steps=result.recipe_1.steps),
                recipe_2=dict(title=result.recipe_2.title, steps=result.recipe_2.steps),
                queries=result.queries
            )
            (save_path / str(idx)).write_text(json.dumps(payload))
    list(map(process, enumerate(separated)))
    #with multiprocessing.ProcessPool(args.processes) as p:
        #p.map(process, enumerate(separated))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(prog='foodcom.py')
    parser.add_argument('--path', '-p', type=Path, help='path to food.com dataset raw CSV', required=True)
    parser.add_argument('--output', '-o', type=Path, help='output folder', required=True)
    parser.add_argument('--num_recipes', '-r', type=int, help='number of recipes to use', default=600)
    parser.add_argument('--num_clusters', '-c', type=int, help='number of clusters', default=50)
    parser.add_argument('--processes', '-proc', type=int, help='processes to use', default=1)
    main(parser.parse_args())
